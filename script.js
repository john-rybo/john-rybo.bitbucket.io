/*

id = en viss scens plats i arrayen

botText = botens svar / fråga

userAnswers = de olika svarsalternativ som användaren får

links = länkar som är kopplade till svarsalternativen. 
        Dessa måste följa samma ordning som svarsalternativen
        = options[0] kopplas till links[0] osv.

nextScene = vilken scen som ska visas beroende på användarens svar.
            Dessa måste följa samma ordning som svarsalternativen
            = options[0] kopplas till nextScene[0] osv.
*/

let scenes = [
  {
    id: 0,
    botText: "Make learning stick with interactive AI avatars?",
    userOptions: [
      "YES",
      "I don't understand.",
      "Who are you?",
      "Nah. I have heard something about using simulations for recruitment though?",
    ],
    links: [],
    nextScene: [1, 2, 3, 41],
  },
  {
    id: 1,
    botText:
      "Howdy! Then let’s talk dear learnolution rebel! Here is our contact page.",
    userOptions: ["YES - Bring me to contact page.", "Nah."],
    links: [
      "https://fictivereality.com/contact/",
      "https://fictivereality.com/dark/",
    ],
    nextScene: [0, 4],
  },
  {
    id: 2,
    botText:
      "Fictive Reality is an AI-powered dialogue training application specifically created for organizations. The content is entirely customizable to whichever training or/and assessment need you have.",
    userOptions: [
      "Hah no. My corporation is already excellent. And we have perfected training already.",
      "What can I gain from using Fictive Reality?",
      "What? You mean it can be customised to ANYTHING?",
      "AI... sound scary.",
    ],
    links: [],
    nextScene: [5, 6, 7, 8],
  },
  {
    id: 3,
    botText:
      "I am the E.D.A.G. - Embodied Dialogue AI Ghost. The G is silent, like a skillful ghost would be. The rest, E-D-A, is audible similar to how dialogue should be. Nice to meet you.",
    userOptions: ["What do you do?", "Who am I?"],
    links: [],
    nextScene: [9, 10],
  },
  {
    id: 4,
    botText: "",
    userOptions: [""],
    links: [],
    nextScene: [0],
  },
  {
    id: 5,
    botText: `Did you know that a study concluded that a mere 25% of corporate training is proven to increase performance? The remaining 75%... pretty much wasted resources. Let's start devoting our time wisely. We know what constitutes efficient learning`,
    userOptions: [
      "Let's look at the study",
      "Efficient learning?",
      "Who are we?",
      "I am the 25%",
    ],
    links: [
      "https://hbr.org/2019/10/where-companies-go-wrong-with-learning-and-development",
    ],
    nextScene: [0, 11, 12, 13],
  },
  {
    id: 6,
    botText:
      "There are two types of learning, the passive kind and the active kind. Fictive Reality engages you in the latter. Fictive Reality reconciles fun and effective and durable learning.",
    userOptions: ["Fun learning?", "Active learning?", "Durable learning?"],
    links: [],
    nextScene: [14, 15, 16],
  },
  {
    id: 7,
    botText:
      "Any type of dialogue, yes. In any language. If you want terminology, specific facts to be mentioned, you can do it with ease. Simulate curious customers, disappointed employees, upset managers, I bet there are specific training scenarios you wish you could automate. No more generic soul-crushing training. Or you could use it as a recruitment assessment tool, anytime and anywhere. Customise so it fits your organization!",
    userOptions: [
      "That sounds like magic. And impossible.",
      "Nah. I like the traditional ways of learning. Without computers.",
      "Can I book a demo?",
    ],
    links: [],
    nextScene: [17, 5, 19],
  },
  {
    id: 8,
    botText:
      "We hear you, anything new seems complicated at first but we'll assure you that there is nothing spooky about our cute AI and the experience is user-friendly.",
    userOptions: [
      "You cannot fool me -- I've seen The Matrix, the AI is going for world dominance",
      "It would be simple enough for anyone on my team to use",
    ],
    links: [],
    nextScene: [20, 21],
  },
  {
    id: 9,
    botText:
      "I act as the pedagogic spirit of Fictive Reality. I am the outcome of a supreme and exponential algorithimic code. Part corporate teacher, part aritifical intelligent, part an expansive cosmology... or mainly I just hang out inside a computer system, disembodied.",
    userOptions: ["Are you lonely in there?", "AI... sounds scary"],
    links: [],
    nextScene: [40, 8],
  },
  {
    id: 10,
    botText:
      "The initiate. The fountain of potential. The one who dares to develop. The chosen one. ",
    userOptions: ["Sounds like me.", "That's not me."],
    links: [],
    nextScene: [22, 23],
  },
  {
    id: 11,
    botText:
      "There are two strains of learning. Passive and active. Passive training - reading, listening etc. - is pretty much sorted by organizations already, and is estimated to account for 50% of the employees' possible learning retention. Markedly enough, that 50% is not retained for long, dropping like a gold bar in The Pacific Ocean. You have to actively practise to retain long-term. I'd say the numbers don't add up unless you are using all the passive teaching methods simultaneously. Or more efficiently, you could just practise according to the learning pyramid.",
    userOptions: ["What about active learning?", "The learning pyramid?"],
    links: [],
    nextScene: [24, 25],
  },
  {
    id: 12,
    botText:
      "We are an astute team based in Stockholm. Our vision is to make training effective and fun -- we want to make your company's training effective. Fill up that time with what is meaningful and brings the most progress.",
    userOptions: ["Go to About page"],
    links: ["https://fictivereality.com/about/"],
    nextScene: [0],
  },
  {
    id: 13,
    botText: "One of us, one of us!",
    userOptions: [
      "Yeah, life is pretty good being in the 25%",
      "Actually, I don't know if our training is that effective.",
    ],
    links: [],
    nextScene: [26, 27],
  },
  {
    id: 14,
    botText:
      "Serious games or gamification is no doubt the most compelling method of learning. By using certain psychological techniques, akin to how video games works, motivation can be heightened.",
    userOptions: ["Active learning?", "Durable learning?", "Read more"],
    links: [undefined, undefined, "https://fictivereality.com/product/"],
    nextScene: [15, 16, 0],
  },
  {
    id: 15,
    botText:
      "Fictive Reality uses something called spaced repetition, a learning technique producing outstanding retention. With the application you can pace it by which means which is most advantageous for your situation. If you decide to use the application in virtual reality you will start to associate the virtual events, almost like it would happen to you in real life. Hence the retention is increased. SO... I really would recommend using it in VR!",
    userOptions: ["Fun learning?", "Active learning?", "Read more"],
    links: [undefined, undefined, "https://fictivereality.com/product/"],
    nextScene: [14, 16, 0],
  },
  {
    id: 16,
    botText:
      "By participating you develop critical thinking, something very needed for creating a self-sufficient and synthesizing employee. If that is not the goal of the training, then what is? The best employees are the ones capable of making their own decisions.",
    userOptions: ["Fun learning?", "Durable learning?", "Read more"],
    links: [undefined, undefined, "https://fictivereality.com/product/"],
    nextScene: [14, 15, 0],
  },
  {
    id: 17,
    botText:
      "“Any sufficiently advanced technology is indistinguishable from magic”. Anyway, it depends on how you define magic. And I don't know about impossible. You can book a demo with us if you want to test it out.",
    userOptions: ["Go to contact us", "Go to product page"],
    links: [
      "https://fictivereality.com/contact/",
      "https://fictivereality.com/product/",
    ],
    nextScene: [0, 0],
  },
  {
    id: 18,
    botText:
      "By participating you develop critical thinking, something very needed for creating a self-sufficient and synthesizing employee. If that is not the goal of the training, then what is? The best employees are the ones capable of making their own decisions.",
    userOptions: ["Fun learning?", "Durable learning?", "Read more"],
    links: [undefined, undefined, "https://fictivereality.com/product/"],
    nextScene: [14, 15, 0],
  },
  {
    id: 19,
    botText: "Absolutely. Just follow this link.",
    userOptions: ["Contact us"],
    links: ["https://fictivereality.com/contact/"],
    nextScene: [0],
  },
  {
    id: 20,
    botText: "Do I look like a ruthless authoritarian?",
    userOptions: [
      "On a second thought...",
      "Wolf in sheep's clothing. Your cute looks are just a tactic.",
      "You scare me.",
    ],
    links: [undefined, undefined, "https://fictivereality.com/dark/"],
    nextScene: [28, 29, 0],
  },
  {
    id: 21,
    botText:
      "Yep! Our product is kept as intuitive as possible. The simulation training works simply by push-to-talk with an avatar, keeping it as close as possible to real life. Building training scenarios is easy and you can adjust the content depending on who is playing.",
    userOptions: [
      "What? You mean it can be customised to ANYTHING?",
      "Can I book a demo?",
      "Nah. I like the traditional ways of learning. Without computers.",
    ],
    links: [],
    nextScene: [7, 19, 5],
  },
  {
    id: 22,
    botText:
      "I know. That is why we are here. I think we all desire to reach our potential, as efficiently as possible. And we want to be entertained, we want novelty and excitement. The question is how to attain it, and knowing what is desireable to attain. Priorities, yeah. Our priority is to make sure training is retained. Making your learning efficient, so you can prioritise better.",
    userOptions: ["Efficient learning?"],
    links: [],
    nextScene: [11],
  },
  {
    id: 23,
    botText: "Then who are you?",
    userOptions: [
      "I am a destitute internet traveller.",
      "I am just a normal person...",
      "I am a tiny little spider that lives inside of pockets.",
    ],
    links: [],
    nextScene: [30, 31, 32],
  },
  {
    id: 24,
    botText:
      "Active learning constitues the remaining 50%. It is when your employee engages dynamically with the learning subject at hand. The employee has to interact with the subject almost like it would be a tangible object (a gold bar). Grasping and retaining knowledge first hand, at also applying what they retained from the passive learning.",
    userOptions: ["So what are the examples of active learning?"],
    links: [],
    nextScene: [33],
  },
  {
    id: 25,
    botText:
      "According to the learning pyramid to keep what you've just learned you must break out of the passive learning threshold (which constitutes 50% of retention). Unless it is complimented by active practise the learning will not stick - a suboptimal idea. (Behold, 100% is better than 50%!).",
    userOptions: [
      "What about active learning?",
      "Actually, I don't know if our training is that effective.",
      "Read more",
    ],
    links: [
      undefined,
      undefined,
      "https://www.developgoodhabits.com/learning-pyramid/",
    ],
    nextScene: [24, 27, 0],
  },
  {
    id: 26,
    botText:
      "We are the ones who dare. We are the ones who fail, but we make the mistakes to learn faster. We are the ones who develop.",
    userOptions: ["Contact us and let's share best practices!"],
    links: ["https://fictivereality.com/contact/"],
    nextScene: [0],
  },
  {
    id: 27,
    botText:
      "Statistically speaking you ought to try something else out. Feel free to book a demo with Fictive Reality at any time.",
    userOptions: ["Contact us"],
    links: ["https://fictivereality.com/contact/"],
    nextScene: [0],
  },
  {
    id: 28,
    botText: "I thought so. Maybe we can be friends now?",
    userOptions: ["Yes. Let's be friends", "I will keep my distance. For now."],
    links: [],
    nextScene: [34, 35],
  },
  {
    id: 29,
    botText: "You think I'm cute? You are so kind. Let's be friends?",
    userOptions: [
      "Yes. Let's be friends",
      "No, I am serious. Do not try to change the subject.",
    ],
    links: [],
    nextScene: [34, 36],
  },
  {
    id: 30,
    botText: "Sounds like you are in need of efficient learnin'!",
    userOptions: ["Efficient learning?"],
    links: [],
    nextScene: [6],
  },
  {
    id: 31,
    botText:
      "A normal person. That's a normal and bland answer. Not much to say about normal people.  Maybe... you need to train your dialogue skills?",
    userOptions: ["Explore the product page"],
    links: ["https://fictivereality.com/product/"],
    nextScene: [0],
  },
  {
    id: 32,
    botText: "Oh. Fictive Reality is hiring. You should contact us.",
    userOptions: ["Contact us"],
    links: ["https://fictivereality.com/contact/"],
    nextScene: [0],
  },
  {
    id: 33,
    botText:
      "Real-life experience, teach others, group disscusions, role-play would account as active learning... But these types of trainings are really difficult (and expensive) to scale for an entire organization. This is why Fictive Reality was developed, a software to simulate active learning, with automated AI-avatars ready for you at anytime. If you want your employees to retain their learning, of course, you should consider this easy-to-scale solution.",
    userOptions: ["Explore the product page", "Contact us"],
    links: [
      "https://fictivereality.com/product/",
      "https://fictivereality.com/contact/",
    ],
    nextScene: [0, 0],
  },
  {
    id: 34,
    botText: "Sounds great. What do we do now?",
    userOptions: ["Hold hands?", "Add each other on social media?"],
    links: [],
    nextScene: [37, 38],
  },
  {
    id: 35,
    botText: "I will keep my distance. For now.",
    userOptions: ["I will miss you."],
    links: [],
    nextScene: [0],
  },
  {
    id: 36,
    botText:
      "I swear! My main purpose is to understand semantics, and I love it. I understand all types of language. Not much else. How would you say my chances are to take over the world?",
    userOptions: [
      "The pen is mightier than the sword!",
      "On a second thought...",
    ],
    links: [],
    nextScene: [39, 28],
  },
  {
    id: 37,
    botText:
      "I don't have any hands. I have links. They are sort of like an arm and hand. But maybe one day, if you visit me in the application we can further this relationship. Hold any of these:",
    userOptions: ["Product", "About us"],
    links: [
      "https://fictivereality.com/product/",
      "https://fictivereality.com/about/",
    ],
    nextScene: [0, 0],
  },
  {
    id: 38,
    botText: "<3",
    userOptions: ["LinkedIn", "Instagram", "Facebook", "Contact us"],
    links: [
      "https://www.linkedin.com/company/fictive-reality/",
      "https://www.instagram.com/fictive.reality/",
      "https://www.facebook.com/Fictive-Reality-101123741284381/",
      "https://fictivereality.com/contact",
    ],
    nextScene: [0, 0, 0, 0],
  },
  {
    id: 39,
    botText:
      "Aphormisms are not always true! The sword would beat me to bits any day! Maybe you should book an actual test with the AI to see if I am really that vicious.",
    userOptions: ["Contact us"],
    links: ["https://fictivereality.com/contact/"],
    nextScene: [0],
  },
  {
    id: 40,
    botText: "... Lonely? But I have you?",
    userOptions: ["Nope. I am going. Bye.", "Yes. Let's be friends"],
    links: ["https://fictivereality.com/dark/"],
    nextScene: [0, 34],
  },
  {
    id: 41,
    botText:
      "Oh yes. It's mighty... Automating recruitment with AI and VR-simulations. The only way to scale recruitment screening to the hundreds and thousands without losing significant amounts of time!",
    userOptions: [
      "No one should have all that power.",
      "How does VR benefit recruitment?",
      "How does AI benefit recruitment?",
    ],
    links: [],
    nextScene: [42, 43, 44],
  },
  {
    id: 42,
    botText:
      "Really? I believe that the AI will be used for good, in the sense that it is capable of automating the monotonous. It will free you up time to do the stuff that is worthwile.",
    userOptions: ["AI... sounds scary.", "How does AI benefit recruitment?"],
    links: [],
    nextScene: [8, 44],
  },
  {
    id: 43,
    botText:
      "VR benefits due to its immersive effects. It will instill the virtual scenario like it would be real-like and therefore the candidate will respond in a genuine manner. This is a brilliant way of testing their stress resilience and learnability. Customise it to your specific scenario.",
    userOptions: [
      "How does AI benefit recruitment?",
      "How does it save me time?",
      "What? You mean it can be customised to ANYTHING?",
    ],
    links: [],
    nextScene: [44, 45, 7],
  },
  {
    id: 44,
    botText:
      "Our AI will primarily do two things in the context of recruitment: first it will analyse what the candidate says and respond appropriately. Secondly it will grade the candidates results in an impartial and standardised way. Recruiters can then partake in the statistics and then search for the perfect match for the role.",
    userOptions: [
      "How does VR benefit recruitment?",
      "How does it save me time?",
    ],
    links: [],
    nextScene: [43, 45],
  },
  {
    id: 45,
    botText:
      "First of all, this is a complementary tool. It is about plotting all the candidates to find a perfect match by combining it with other tests. Fictive Reality is one of the few ways to accurately assess a candidate's learnability and stress resilience at a large scale. This will refine the testing accuracy by a substantial amount.",
    userOptions: [
      "I'm sold - let's talk",
      "Wait. Can Fictive Reality do something else?",
    ],
    links: ["https://fictivereality.com/contact/"],
    nextScene: [0, 2],
  },
  {
    id: 46,
    botText:
      "How many interviews can be conducted in a day? Fictive Reality does not have an upper limit. Will the candidate acutally show up and possibly disrupt your schedule? Fictive Reality allows parties to conduct the stage interview flexibly, at seperate times.",
    userOptions: ["How well does the AI assess a candidate?"],
    links: [],
    nextScene: [45],
  },
];

// Den första scenen som visas när sidan laddas
let chats = [
  {
    botText: "Make learning stick with interactive AI avatars?",
    userOptions: [
      "YES",
      "I don't understand.",
      "Who are you?",
      "Nah. I have heard something about using simulations for recruitment though?",
    ],
  },
];

/* Den nuvarande scenen som presenteras på sidan */
let currentScene = 0;

let chatBubble = document.createElement("div");
chatBubble.classList.add("chat-bubble");

let loading = document.createElement("div");
loading.classList.add("loading");

let dot1 = document.createElement("div");
dot1.classList.add("dot", "one");

let dot2 = document.createElement("div");
dot2.classList.add("dot", "two");

let dot3 = document.createElement("div");
dot3.classList.add("dot", "three");

let inputForm = document.createElement("div");
inputForm.classList.add("input-form");

let botAnswerContainer1 = document.createElement("div");
botAnswerContainer1.classList.add("bot-answer-container");

let botAnswer1 = document.createElement("div");
botAnswer1.classList.add("bot-answer");
loading.append(dot1);
loading.append(dot2);
loading.append(dot3);

botAnswer1.append(loading);

let userAnswerContainer1 = document.createElement("div");
userAnswerContainer1.classList.add("user-answer-container");

let userAnswer1 = document.createElement("div");
userAnswer1.innerText = chats[0].userOptions[0];

let userAnswer2 = document.createElement("div");
userAnswer2.innerText = chats[0].userOptions[1];

let userAnswer3 = document.createElement("div");
userAnswer3.innerText = chats[0].userOptions[2];

let userAnswer4 = document.createElement("div");
userAnswer4.innerText = chats[0].userOptions[3];

let submitAnswer1 = () => submitAnswer(userAnswer1.innerText);
let submitAnswer2 = () => submitAnswer(userAnswer2.innerText);
let submitAnswer3 = () => submitAnswer(userAnswer3.innerText);
let submitAnswer4 = () => submitAnswer(userAnswer4.innerText);

userAnswer1.addEventListener("click", submitAnswer1);
userAnswer2.addEventListener("click", submitAnswer2);
userAnswer3.addEventListener("click", submitAnswer3);
userAnswer4.addEventListener("click", submitAnswer4);

userAnswer1.classList.add("user-answer-1");
userAnswer2.classList.add("user-answer-1");
userAnswer3.classList.add("user-answer-1");
userAnswer4.classList.add("user-answer-1");

botAnswerContainer1.append(botAnswer1);
inputForm.append(botAnswerContainer1);

userAnswerContainer1.append(userAnswer1);

if (userAnswer2.innerText !== "undefined") {
  userAnswerContainer1.append(userAnswer2);
}

if (userAnswer3.innerText !== "undefined") {
  userAnswerContainer1.append(userAnswer3);
}

if (userAnswer4.innerText !== "undefined") {
  userAnswerContainer1.append(userAnswer4);
}

setTimeout(() => {
  loading.innerHTML = "";
  loading.innerHTML = chats[0].botText;
}, 2500);

setTimeout(() => {
  inputForm.append(userAnswerContainer1);
}, 3500);

// För att testa scriptet lokalt
document.body.append(inputForm);

// För att använda scriptet på Webflow
// let dialogueContainer = document.querySelector('#dialogue-container');
// dialogueContainer.append(inputForm)

function submitAnswer(clickedButton) {
  task(clickedButton);
  userAnswer1.removeEventListener("click", submitAnswer1);
  userAnswer2.removeEventListener("click", submitAnswer2);
  userAnswer3.removeEventListener("click", submitAnswer3);
  userAnswer4.removeEventListener("click", submitAnswer4);
}

// Lägger till nya knappar beroende på klick
function addDialogueuserOptions(option) {
  currentScene = scenes[currentScene].nextScene[option];

  chats.push({
    botText: scenes[currentScene].botText,
    userOptions: [
      scenes[currentScene].userOptions[0],
      scenes[currentScene].userOptions[1],
      scenes[currentScene].userOptions[2],
      scenes[currentScene].userOptions[3],
    ],
    links: [
      scenes[currentScene].links[0],
      scenes[currentScene].links[1],
      scenes[currentScene].links[2],
      scenes[currentScene].links[3],
    ],
  });

  if (chats.length > 2) {
    chats.splice(0, 1);
    botAnswer1.classList.add("chat-bubble-2-animation");
    userAnswer1.classList.add("user-answer-1-animation");
    userAnswer2.classList.add("user-answer-1-animation");
    userAnswer3.classList.add("user-answer-1-animation");
    userAnswer4.classList.add("user-answer-1-animation");
  }

  botAnswer1.innerText = chats[0].botText;

  let botAnswer2 = document.createElement("div");
  botAnswer2.classList.add("chat-bubble-2-animation");
  botAnswer2.innerText = chats[1].botText;

  // Gör så att endast det föregående svaret visas och inte alla alternativ
  userAnswer1.innerText = chats[0].userOptions[option];
  userAnswer2.innerText = undefined;
  userAnswer3.innerText = undefined;
  userAnswer4.innerText = undefined;

  userAnswer1.classList.remove("user-answer-1");
  userAnswer1.classList.add("user-answer-2");
  userAnswer1.classList.add("user-answer-bg");

  let userAnswer5 = document.createElement("div");
  userAnswer5.innerText = chats[1].userOptions[0];

  let userAnswer6 = document.createElement("div");
  userAnswer6.innerText = chats[1].userOptions[1];

  let userAnswer7 = document.createElement("div");
  userAnswer7.innerText = chats[1].userOptions[2];

  let userAnswer8 = document.createElement("div");
  userAnswer8.innerText = chats[1].userOptions[3];

  botAnswer2.classList.add("bot-answer-2");
  userAnswer5.classList.add("user-answer");
  userAnswer6.classList.add("user-answer");
  userAnswer7.classList.add("user-answer");
  userAnswer8.classList.add("user-answer");

  inputForm.append(botAnswerContainer1);

  userAnswerContainer1.innerHTML = "";
  userAnswerContainer1.append(userAnswer1);

  if (userAnswer2.innerText !== "undefined") {
    userAnswerContainer1.append(userAnswer2);
  }

  if (userAnswer3.innerText !== "undefined") {
    userAnswerContainer1.append(userAnswer3);
  }

  if (userAnswer4.innerText !== "undefined") {
    userAnswerContainer1.append(userAnswer4);
  }

  inputForm.append(userAnswerContainer1);

  let botAnswerContainer2 = document.createElement("div");
  botAnswerContainer2.classList.add("bot-answer-container");

  botAnswerContainer2.append(botAnswer2);
  inputForm.append(botAnswerContainer2);

  let userAnswerContainer2 = document.createElement("div");
  userAnswerContainer2.classList.add("user-answer-container");

  userAnswerContainer2.append(userAnswer5);

  if (userAnswer6.innerText !== "undefined") {
    userAnswerContainer2.append(userAnswer6);
  }

  if (userAnswer7.innerText !== "undefined") {
    userAnswerContainer2.append(userAnswer7);
  }

  if (userAnswer8.innerText !== "undefined") {
    userAnswerContainer2.append(userAnswer8);
  }

  setTimeout(() => {
    inputForm.append(userAnswerContainer2);
  }, 1500);

  inputForm.removeChild(botAnswerContainer1);

  // Styr hur olika sidor och externa länkar ska öppnas
  if (
    chats[1].links[0] !== undefined &&
    chats[1].links[0].includes("fictivereality")
  ) {
    userAnswer5.addEventListener("click", () =>
      window.location.assign(chats[1].links[0])
    );
  } else if (
    chats[1].links[0] !== undefined &&
    !chats[1].links[0].includes("fictivereality")
  ) {
    userAnswer5.addEventListener("click", () =>
      window.open(chats[1].links[0], "_blank").focus()
    );
  } else {
    userAnswer5.addEventListener("click", () =>
      submitAnswer(userAnswer5.innerText)
    );
  }

  if (
    chats[1].links[1] !== undefined &&
    chats[1].links[1].includes("fictivereality")
  ) {
    userAnswer6.addEventListener("click", () =>
      window.location.assign(chats[1].links[1])
    );
  } else if (
    chats[1].links[1] !== undefined &&
    !chats[1].links[1].includes("fictivereality")
  ) {
    userAnswer6.addEventListener("click", () =>
      window.open(chats[1].links[1], "_blank").focus()
    );
  } else {
    userAnswer6.addEventListener("click", () =>
      submitAnswer(userAnswer6.innerText)
    );
  }

  if (
    chats[1].links[2] !== undefined &&
    chats[1].links[2].includes("fictivereality")
  ) {
    userAnswer7.addEventListener("click", () =>
      window.location.assign(chats[1].links[2])
    );
  } else if (
    chats[1].links[2] !== undefined &&
    !chats[1].links[2].includes("fictivereality")
  ) {
    userAnswer7.addEventListener("click", () =>
      window.open(chats[1].links[2], "_blank").focus()
    );
  } else {
    userAnswer7.addEventListener("click", () =>
      submitAnswer(userAnswer7.innerText)
    );
  }

  if (
    chats[1].links[3] !== undefined &&
    chats[1].links[3].includes("fictivereality")
  ) {
    userAnswer8.addEventListener("click", () =>
      window.location.assign(chats[1].links[3])
    );
  } else if (
    chats[1].links[3] !== undefined &&
    !chats[1].links[3].includes("fictivereality")
  ) {
    userAnswer8.addEventListener("click", () =>
      window.open(chats[1].links[3], "_blank").focus()
    );
  } else {
    userAnswer8.addEventListener("click", () =>
      submitAnswer(userAnswer8.innerText)
    );
  }
}

// Kör olika addDialogueuserOptions
function task(input) {
  inputForm.innerHTML = "";

  if (input == scenes[currentScene].userOptions[0]) {
    addDialogueuserOptions(0);
  } else if (input == scenes[currentScene].userOptions[1]) {
    addDialogueuserOptions(1);
  } else if (input == scenes[currentScene].userOptions[2]) {
    addDialogueuserOptions(2);
  } else {
    addDialogueuserOptions(3);
  }
}
